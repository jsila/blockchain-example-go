package main

import (
	"encoding/json"
	"time"
)

type Block struct {
	Index        int          `json:"index"`
	Timestamp    time.Time    `json:"timestamp"`
	Transactions Transactions `json:"transaction"`
	Proof        int          `json:"proof"`
	PreviousHash string       `json:"previous_hash"`
}

func (b *Block) hash() (string, error) {
	s, err := json.Marshal(b)
	if err != nil {
		return "", err
	}
	return generateHash(s), nil
}

func NewBlock(bc *Blockchain, proof int, previousHash string) *Block {
	b := &Block{
		Index:        len(bc.Chain) + 1,
		Timestamp:    time.Now(),
		Transactions: bc.CurrentTransactions,
		Proof:        proof,
		PreviousHash: previousHash,
	}
	return b
}

type Chain []*Block

func (c Chain) IsValid() (bool, error) {
	lastBlock := c[0]

	for i := 1; i < len(c); i++ {
		b := c[i]

		blockHash, err := lastBlock.hash()
		if err != nil {
			return false, err
		}

		if blockHash != b.PreviousHash {
			return false, nil
		}

		if !validateProof(lastBlock.Proof, b.Proof) {
			return false, nil
		}

		lastBlock = b
	}
	return true, nil
}
