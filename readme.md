Go port of Python app built in [Learn Blockchains by Building One](https://hackernoon.com/learn-blockchains-by-building-one-117428612f46) with improved maintainability.

Also it made sense adding concurrency (when resolving conflict via consensus and querying other nodes in a network) so I did that too. :)