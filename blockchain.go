package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sync"
)

type Blockchain struct {
	Nodes               []string
	CurrentTransactions Transactions
	Chain               Chain
	LastBlock           *Block
}

func (bc *Blockchain) AddBlock(previousHash string, proof int) *Block {
	b := NewBlock(bc, proof, previousHash)
	bc.LastBlock = b
	bc.CurrentTransactions = Transactions{}
	bc.Chain = append(bc.Chain, b)
	return b
}

func (bc *Blockchain) AddTransaction(t *Transaction) int {
	bc.CurrentTransactions = append(bc.CurrentTransactions, t)
	return bc.LastBlock.Index + 1
}

type Data struct {
	Err    error
	Length int
	Chain  Chain
}

func (bc *Blockchain) ResolveConflict(nodes []string) (bool, error) {
	chains := make(chan *Data)
	var wg sync.WaitGroup
	wg.Add(len(nodes))

	go func() {
		wg.Wait()
		close(chains)
	}()

	var newChain Chain
	maxLen := len(bc.Chain)

	for _, node := range nodes {
		go func(node string) {
			defer wg.Done()
			resp, err := http.Get(fmt.Sprintf("http://%s/chain", node))

			if err != nil {
				chains <- &Data{Err: err}
				return
			}

			if resp.StatusCode != http.StatusOK {
				chains <- &Data{Err: fmt.Errorf("status code not OK for %s", node)}
				return
			}

			var data Data

			if err := json.NewDecoder(resp.Body).Decode(&data); err != nil {
				chains <- &Data{Err: err}
				return
			}

			chains <- &data

		}(node)
	}

	for d := range chains {
		if d.Err != nil {
			return false, d.Err
		}

		isValid, err := d.Chain.IsValid()
		if err != nil {
			return false, err
		}

		if d.Length > maxLen && isValid {
			maxLen = d.Length
			newChain = d.Chain
		}
	}

	if len(newChain) > 0 {
		bc.Chain = newChain
		return true, nil
	}

	return false, nil
}

func NewBlockchain() *Blockchain {
	b := &Blockchain{
		CurrentTransactions: Transactions{},
	}
	b.AddBlock("1", 100)
	return b
}
