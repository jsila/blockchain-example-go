package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"log"
	"net/http"
	"strings"

	"github.com/go-chi/chi"
	"github.com/matryer/respond"
	uuid "github.com/satori/go.uuid"
)

func main() {
	port := flag.Int("port", 8080, "port to listen to")
	flag.Parse()

	nodeIdentifier := strings.Replace(uuid.NewV4().String(), "-", "", -1)

	var nodes []string
	blockchain := NewBlockchain()

	r := chi.NewRouter()

	r.Get("/chain", func(w http.ResponseWriter, r *http.Request) {
		data := struct {
			Length int   `json:"length"`
			Chain  Chain `json:"chain"`
		}{
			Length: len(blockchain.Chain),
			Chain:  blockchain.Chain,
		}

		respond.With(w, r, http.StatusOK, data)
	})

	r.Post("/transactions", func(w http.ResponseWriter, r *http.Request) {
		t := &Transaction{}

		if err := json.NewDecoder(r.Body).Decode(t); err != nil {
			log.Println(err)
			http.Error(w, "Bad Request", http.StatusBadRequest)
			return
		}

		index := blockchain.AddTransaction(t)

		respond.With(w, r, http.StatusCreated, map[string]string{
			"message": fmt.Sprintf("Transaction will be added to block %v", index),
		})
	})

	r.Post("/mine", func(w http.ResponseWriter, r *http.Request) {
		blockchain.AddTransaction(&Transaction{
			Sender:    "0",
			Recipient: nodeIdentifier,
			Amount:    1,
		})

		blockHash, err := blockchain.LastBlock.hash()
		if err != nil {
			log.Println(err)
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}

		block := blockchain.AddBlock(
			blockHash,
			proofOfWork(blockchain.LastBlock.Proof),
		)

		data := struct {
			*Block
			Message string `json:"message"`
		}{
			Message: "New block forged",
			Block:   block,
		}

		respond.With(w, r, http.StatusOK, data)
	})

	r.Post("/nodes/register", func(w http.ResponseWriter, r *http.Request) {
		var err error
		var data struct {
			Nodes []string
		}

		if err = json.NewDecoder(r.Body).Decode(&data); err != nil {
			log.Println(err)
			http.Error(w, "Bad Request", http.StatusBadRequest)
			return
		}

		nodes, err = addNodes(nodes, data.Nodes)

		if err = json.NewDecoder(r.Body).Decode(&data); err != nil {
			log.Println(err)
			http.Error(w, "Bad Request (can't parse node's url)", http.StatusBadRequest)
			return
		}

		respond.With(w, r, http.StatusCreated, map[string]interface{}{
			"message":     "New nodes have been added",
			"total_nodes": nodes,
		})
	})

	r.Post("/nodes/resolve", func(w http.ResponseWriter, r *http.Request) {
		replaced, err := blockchain.ResolveConflict(nodes)

		if err != nil {
			log.Println(err)
			http.Error(w, "Internal server error", http.StatusInternalServerError)
			return
		}

		data := struct {
			Message string `json:"message"`
			Chain   Chain  `json:"chain"`
		}{
			Message: "Our chain is authoritative",
			Chain:   blockchain.Chain,
		}

		if replaced {
			data.Message = "Our chain was replaced"
		}
		respond.With(w, r, http.StatusOK, data)
	})

	fmt.Printf("Listening on port %v", *port)
	http.ListenAndServe(fmt.Sprintf(":%v", *port), r)
}
