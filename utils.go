package main

import (
	"crypto/sha256"
	"fmt"
	"net/url"
)

func generateHash(b []byte) string {
	sha := sha256.New()
	sha.Write(b)
	return fmt.Sprintf("%x", sha.Sum(nil))
}

func validateProof(lastP, p int) bool {
	guess := fmt.Sprintf("%v%v", lastP, p)

	return generateHash([]byte(guess))[:4] == "0000"
}

func proofOfWork(lastP int) int {
	p := 0
	for !validateProof(lastP, p) {
		p++
	}
	return p
}

func addNodes(nodes, newNodes []string) ([]string, error) {
	for _, node := range newNodes {
		parsed, err := url.Parse(node)
		if err != nil {
			return nil, err
		}
		nodes = appendIfUnique(nodes, parsed.Host)
	}
	return nodes, nil
}

func appendIfUnique(nodes []string, node string) []string {
	for _, n := range nodes {
		if n == node {
			return nodes
		}
	}
	return append(nodes, node)
}
